# Luke's build of dwm (patched for my use)

## FAQ

> What are the bindings?

This is suckless, mmmbud, the source code is the documentation! Check out [config.h](config.h).

Okay, okay, actually I keep a readme in `larbs.mom` for my whole system, including the binds here.
Press `super+F1` to view it in dwm (zathura is required for that binding).
I haven't kept `man dwm`/`dwm.1` updated though. PRs welcome on that, lol.

## Patches and features

- Clickable statusbar with my build of [dwmblocks](https://github.com/lukesmithxyz/dwmblocks).
- Reads xresources colors/variables (i.e. works with `pywal`, etc.).
- scratchpad: Accessible with mod+shift+enter.
- New layouts: bstack, fibonacci, deck, centered master and more. All bound to keys `super+(shift+)t/y/u/i`.
- True fullscreen (`super+f`) and prevents focus shifting.
- Windows can be made sticky (`super+s`).
- stacker: Move windows up the stack manually (`super-K/J`).
- shiftview: Cycle through tags (`super+g/;`).
- vanitygaps: Gaps allowed across all layouts.
- swallow patch: if a program run from a terminal would make it inoperable, it temporarily takes its place to save space.

## Please install `libxft-bgra`!

This build of dwm does not block color emoji in the status/info bar, so you must install [libxft-bgra](https://aur.archlinux.org/packages/libxft-bgra/) from the AUR, which fixes a libxft color emoji rendering problem, otherwise dwm will crash upon trying to render one. Hopefully this fix will be in all libxft soon enough.

## My additions:

- Floating windows start in the [center of screen](https://dwm.suckless.org/patches/center/)
- Bar is hidden, but not disabled. If you disable the bar the windows will overlap with the xfce4-panel or whatever panel you are using.
- Some applications are floating by default now, like file-roller.
- Switched some application shortcuts around, Mod+d now opens whiskermenu, Mod+r opens thunar, basically xfce'fied the whole setup.

## Future plans:

- Might start using #define's for default shortcuts, to make it easier to change applications.

## Things I couldn't do:

- Disable the bar without overlapping, need to find a way to force windows to go down a set number of pixels.
- Make whiskermenu float.
